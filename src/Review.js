import React, { useState } from 'react';
import reviews from './Data';
import './App.css';
import { RiArrowLeftSLine, RiArrowRightSLine, RiDoubleQuotesR } from 'react-icons/ri'

const Review = () => {
    const [index, setIndex] = useState(2);
    const { id, name, job, image, text } = reviews[index]

    const checkNumber = (number) => {
      if(number > reviews.length-1){
        return 0;
      }
      if(number < 0){
        return reviews.length-1;
      }
      return number;
    }

    const nextPerson = () => {
      setIndex((index) => {
        let newIndex = index + 1;
        return checkNumber(newIndex);
      })
    }

    const prevPerson = () => {
      setIndex((index) => {
        let newIndex = index - 1;
        return checkNumber(newIndex);
      })
    }
  return (
    <article className='review'>
      <div className='img-container'>
        <img src={image} alt={name} className="person-img"/>
        <span className='quote-icon'>
             <RiDoubleQuotesR/>
        </span>
      </div>
      <h4 className='author'>{name}</h4>
      <p className='job'>{job}</p>
      <p className='info'>{text}</p>
      <div className='button-container'>
        <button className='prev-btn' onClick={nextPerson}>
          <RiArrowLeftSLine/>
        </button>
        <button className='next-btn' onClick={prevPerson}>
          <RiArrowRightSLine/>
        </button>
      </div>
      <button className='random-btn'>suprise me</button>
    </article>
  )
}

export default Review
